// Import main css
import '~/assets/style/index.scss'

// Import default layout so we don't need to import it to every page
import DefaultLayout from '~/layouts/Default.vue'

// The Client API can be used here. Learn more: gridsome.org/docs/client-api
export default function (Vue, { router, head, isClient }) {

  // Set default layout as a global component
  Vue.component('Layout', DefaultLayout)
  Vue.filter('slugable', function(value) {
  	value = value.toLowerCase();
      // faz as substituições dos acentos
      value = value.replace(/[á|ã|â|à]/gi, "a");
      value = value.replace(/[é|ê|è]/gi, "e");
      value = value.replace(/[í|ì|î]/gi, "i");
      value = value.replace(/[õ|ò|ó|ô]/gi, "o");
      value = value.replace(/[ú|ù|û]/gi, "u");
      value = value.replace(/[ç]/gi, "c");
      value = value.replace(/[ñ]/gi, "n");
      value = value.replace(/[á|ã|â]/gi, "a");
      // faz a substituição dos espaços e outros caracteres por - (hífen)
      value = value.replace(/\W/gi, "-");
      // remove - (hífen) duplicados
      value = value.replace(/(\-)\1+/gi, "-");
      return value;
  });
}
