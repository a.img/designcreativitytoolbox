---
title: Course setup
week: week 1
hint: false
number: "3"
description: The setup of this course is similar to the setup of Interaction
  Design 1. Here you can read more about this.
---
The setup of this course is similar to the setup of Interaction Design 1. Here you can read more about this.

### Two parts in the course

The first 4 weeks of this course, you will learn and explore interaction design principles and interaction design patterns by working on individual assignments.  In week 5 of the course, the final assignment will be presented to you. You will work on this final assignment in pairs, until the end of the course.

### Online setup of the course

Due to the Coronacrisis the course will be entirely online. The setup is as follows: 

#### Contents course

* Interaction Design patterns are explained in the following book, that is used in this course: Designing Interfaces - 2nd edition (Patterns for Effective Interaction Design) - Jenifer Tidwell.
* If you did not buy this book yet, you can also buy the newly published 3rd edition. In general, contents are the same. There are some minor changes in the patterns, but both books are good to use in this course. 
* You are expected to study the patterns in this book yourself. Like you are used in the course Interaction Design 1, there will be no lectures about the patterns. But of course tutor meetings allow for discussion and questions about the topics that you studied. 
* All course contents and assignments will be published on this website. This website is also used to explain which chapters from the book should be studied before each lesson and to explain the assignments you need to work on.



#### Tutor groups and tutor meetings

* The classes will be divided into tutor groups of 5-7 students. The lecturer will set up these groups. We advise you to use the tutor groups to learn from and inspire each other. Tutor groups can be changed for the second part of the course (when you work on the final assignment). 
* In week 1 of the course there will be a kickoff lesson. You are expected to prepare for this lesson by studying materials that will be communicated to you by a Blackboard announcement. Study these materials before the kickoff lesson. Your lecturer will invite your tutor group for a first tutor meeting that will be scheduled during the kickoff lesson (as planned in your schedule on Wednesday 29 April).
* In week 2 to 8 of the course, two lessons a week are scheduled (with the exception of week 6, for which one lesson is scheduled). During these lessons, the lecturer will have a meeting with each tutor group (20 minutes per tutor group). Your lecturer will let you know when your tutor group is expected for the tutor meetings.
* Tutor meetings allow for discussion and question about the contents of the course. Also, you will get feedback from your lecturer on your assignments, that you will use to improve your work. 
* We highly recommend that you do not only use the tutor meetings to help each other in your tutor group, but that you use the entire scheduled lesson time to interact with each other.

#### Communication channels

* General communication about the course will take place through Blackboard announcements.
* Microsoft Teams will be used for tutor meetings. Lecturers will make chat groups for all tutor groups. You will use the chat groups to share assignments with each other and give each other feedback. Lecturers will use these chat groups for communication with separate tutor groups.