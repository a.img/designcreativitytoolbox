---
title: "Getting around: navigation, signposts, wayfinding"
week: week 2
hint: false
number: "7"
description: To prepare for the first meeting of this week, read Chapter 3 in
  the book (the chapter number is the same for the 2nd and 3rd edition). Here
  you can find a document that will help you structure the information in this
  chapter.
---
To prepare for the first meeting of this week, read Chapter 3 in the book (the chapter number is the same for the 2nd and 3rd edition). Here you can find a document that will help you structure the information in this chapter.



> **Summary of the Chapter**
>
> Download a summary of this chapter [here](https://blackboard.hhs.nl/bbcswebdav/pid-2976800-dt-content-rid-25695692_2/courses/UXD-ID2-1-16-2019/Week%202-Chapter%20Getting%20around-1920.pdf). It will help you to structure the information that you read in this chapter.