---
title: Assignment Webshop Me, myself and I
week: week 3
hint:
  - false
number: "14"
cover_image: ""
description: This week you will apply what you read about Lists of things and
  Filtering, sorting and call-to-action by designing a compact webshop. Here you
  can read we expect you to do.
---
This week you will apply what you read about Lists of things and Filtering, sorting and call-to-action by designing a compact webshop. Here you can read we expect you to do. 

> **Assignment Webshop Me, myself and I**
>
> Download the assignment [here](https://blackboard.hhs.nl/bbcswebdav/pid-2976853-dt-content-rid-25696633_2/courses/UXD-ID2-1-16-2019/Week%203-Assignment%20Webshop%20Me%20Myself%20and%20I-1920.pdf).