---
title: Preparations before kickoff
week: week 1
hint: false
number: "4"
description: Your lecturer will invite you for a kickoff tutor meeting on
  Wednesday 29 April. You are expected to prepare for that tutor meeting. Here
  you can read what you need to prepare.
---
Before the kickoff meeting that will take place at Wednesday 29 April, you are expected to study the following materials, that are all available for download in the grey box.

Start with reading the study guide. After that, download the kickoff presentation and go through it slide by slide. Some slides explain actions you need to take. Do these actions before you continue to the next slide. This way, you will get the best introduction to the course and design principles and design patterns.

After the kickoff meeting, you can start with the assignment for this week. 

> **Must read before the kickoff meeting**
>
> Study guide: login to Blackboard and click [here](https://blackboard.hhs.nl/bbcswebdav/pid-2976660-dt-content-rid-25706578_2/courses/UXD-ID2-1-16-2019/Study%20Guide-ID2-1920%281%29.pdf) to download the study guide.
>
> Presentation kickoff: login to Blackboard and click [here](https://blackboard.hhs.nl/bbcswebdav/pid-2976785-dt-content-rid-25695664_2/courses/UXD-ID2-1-16-2019/Week%201-Kickoff-1920.pdf) to download the presentation. Go through the presentation slide by slide.
>
> Handout design principles: login to Blackboard and click [here](https://blackboard.hhs.nl/bbcswebdav/pid-2976787-dt-content-rid-25695665_2/courses/UXD-ID2-1-16-2019/Week%201-Handout%20principles-1920.pdf) to download the handout. Study the handout so you get an idea of the design principles that we will use in this course.
>
> Assignment week 1: login to Blackboard and click [here](https://blackboard.hhs.nl/bbcswebdav/pid-2976788-dt-content-rid-25695667_2/courses/UXD-ID2-1-16-2019/Week%201-Assignment%20Expert%20review-1920.pdf) to download the assignment. Read the assignment and think of questions that you can ask about the assignment in the first tutor meeting. You will start doing the assignment after this first tutor meeting.