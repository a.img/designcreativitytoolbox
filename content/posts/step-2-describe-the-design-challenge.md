---
title: Step 2 - Describe the design challenge
week: week 6
hint:
  - false
number: "23"
cover_image: ""
description: Write a short description of the design challenge, based on the
  insights you gained in step 1. Describe the client, the target audience, the
  problem statement and the goal.
---
Write a short description of the design challenge, based on the insights you gained in step 1. Describe the client, the target audience, the problem statement and the goal.

Use the following elements to describe the design challenge: 

**Who is the client** 

Give a description of The Dutch Store

**What is the problem statement** 

The problem states why a redesign is necessary

**What is the target audience** 

Describe the target audience, for more information see the persona

**What is the goal of the design challenge** 

What will be achieved with the redesign

The introduction should be no longer than 1 A4.