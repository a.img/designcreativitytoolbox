---
title: Doing things
week: week 4
hint:
  - false
number: "16"
cover_image: ""
description: To prepare for the first meeting of this week, read Chapter 6 in
  the book if you have the 2nd edition or Chapter 8 if you have the 3rd edition.
  Here you can find a document that will help you structure the information in
  this chapter.
---
To prepare for the first meeting of this week, read Chapter 6 in the book if you have the 2nd edition or Chapter 8 if you have the 3rd edition. Here you can find a document that will help you structure the information in this chapter.



> **Summary of the Chapter**
>
> Download a summary of this chapter [here](https://blackboard.hhs.nl/bbcswebdav/pid-2976901-dt-content-rid-25696834_2/courses/UXD-ID2-1-16-2019/Week%204-Chapter%20Doing%20things-1920.pdf). It will help you to structure the information that you read in this chapter.