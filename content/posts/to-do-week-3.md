---
title: "*** TO DO WEEK 3 ***"
week: week 3
hint:
  - false
number: "11"
cover_image: ""
description: Before the first tutor meeting of this week, study Chapter 5 from
  the book if you have the 2nd edition or Chapter 7 if you have the 3rd edition.
  Also study the information about Filtering, sorting and call-to-action.  After
  the meeting, start working on the assignment for this week, that you can find
  at 'Assignment Webshop Me, Myself and I'.
---
Before the first tutor meeting of this week, study Chapter 5 from the book if you have the 2nd edition or Chapter 7 if you have the 3rd edition. Also study the information about Filtering, sorting and call-to-action.  After the meeting, start working on the assignment for this week, that you can find at 'Assignment Webshop Me, Myself and I'.