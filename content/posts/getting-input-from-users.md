---
title: Getting input from users
week: week 4
hint:
  - false
number: "16"
cover_image: ""
description: To prepare for the first meeting of this week, read Chapter 8 in
  the book if you have the 2nd edition or Chapter 10 if you have the 3rd
  edition. Here you can find a document that will help you structure the
  information in this chapter.
---
To prepare for the first meeting of this week, read Chapter 8 in the book if you have the 2nd edition or Chapter 10 if you have the 3rd edition. Here you can find a document that will help you structure the information in this chapter.

> **Summary of the Chapter**
>
> Download a summary of this chapter [here](https://blackboard.hhs.nl/bbcswebdav/pid-2976904-dt-content-rid-25696835_2/courses/UXD-ID2-1-16-2019/Week%204-Chapter%20Getting%20input%20from%20users-1920.pdf). It will help you to structure the information that you read in this chapter.