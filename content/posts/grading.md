---
title: Grading
week: week 10
hint:
  - false
number: "45"
cover_image: ""
description: Here you can find the assessment form that explain what we as
  lecturers will look at when we grade your final assignment.
---
Here you can find the assessment form that explain what we as lecturers will look at when we grade your final assignment.

Download the grading form [here](https://blackboard.hhs.nl/bbcswebdav/pid-2976781-dt-content-rid-25695643_2/courses/UXD-ID2-1-16-2019/Assessmentform-ID2-1920.pdf).