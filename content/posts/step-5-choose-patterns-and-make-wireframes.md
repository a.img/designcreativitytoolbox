---
title: Step 5 - Choose patterns and make wireframes
week: week 7
hint:
  - false
number: "28"
cover_image: ""
description: "Make lo-fi wireframes for the redesign of all relevant pages and
  elements, \vin which you apply patterns from the book: a minimum of \v4
  patterns of each chapter that was discussed in this course."
---
Make lo-fi wireframes for the redesign of all relevant pages and elements, in which you apply patterns from the book: a minimum of 4 patterns of each chapter that was discussed in this course.

## Pages and elements

The lo-fi wireframes can be hand sketched or digital and should consist of the following pages/elements:

**Homepage**

**Product overview page** including pages/functionalities that support browsing through and searching for products (eg filtering/sorting/pagination/etc)

**Product detail page**

**All pages that are part of the check-out process** (don’t forget to include the shopping cart and confirmation page!).

## Use of patterns

In your wireframes, use the patterns you learned in this course. Use a minimum of 4 patterns from each Chapter in the book that was discussed in this course.

## What to present in the report

Present screenshots/pictures of your wireframes in the report and add clear titles to the wireframes.