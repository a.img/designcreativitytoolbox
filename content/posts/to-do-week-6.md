---
title: "*** TO DO WEEK 6***"
week: week 6
hint:
  - false
number: "21"
cover_image: ""
description: To keep on track with the final assignment, we advise you to finish
  the steps presented here in week 6.
---
To keep on track with the final assignment, we advise you to finish the steps presented here in week 6.