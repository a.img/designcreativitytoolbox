---
title: Filtering, sorting and call-to-action
week: week 3
hint:
  - false
number: "13"
cover_image: ""
description: To prepare for the first meeting of this week, next to the chapter
  in the book, also study the information about Filtering, sorting and
  caal-to-action that is presented here.
---
To prepare for the first meeting of this week, next to the chapter in the book, also study the information about Filtering, sorting and caal-to-action that is presented here. 



> **Extra information on filtering, sorting and call-to-action**
>
> Download the extra information [here](https://blackboard.hhs.nl/bbcswebdav/pid-2976852-dt-content-rid-25696630_2/courses/UXD-ID2-1-16-2019/Week%203-Extra%20Filtering%20Sorting%20Call%20to%20action-1920.pdf).