---
title: Lists of things
week: week 3
hint:
  - false
number: "12"
cover_image: ""
description: To prepare for the first meeting of this week, read Chapter 5 in
  the book if you have the 2nd edition or Chapter 7 if you have the 3rd edition.
  Here you can find a document that will help you structure the information in
  this chapter.
---
To prepare for the first meeting of this week, read Chapter 5 in the book if you have the 2nd edition or Chapter 7 if you have the 3rd edition. Here you can find a document that will help you structure the information in this chapter.



> **Summary of the Chapter**
>
> Download a summary of this chapter [here](https://blackboard.hhs.nl/bbcswebdav/pid-2976829-dt-content-rid-25696629_2/courses/UXD-ID2-1-16-2019/Week%203-Chapter%20Lists%20of%20things-1920.pdf). It will help you to structure the information that you read in this chapter.