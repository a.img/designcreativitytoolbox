---
title: "*** TO DO WEEK 5 ***"
week: week 5
hint:
  - false
number: "18"
cover_image: ""
description: This week you have to finish all individual assignments if you want
  to apply for a bonus point. The deadline for these individual assignments is
  Friday in this week (29 May). More information can be found in the box
  'Deadline individual assignments'. This week you will also receive the final
  assignment, that you will be working on in pairs. More information about this
  can be found in the box 'Final assignment'.
---
This week you have to finish all individual assignments if you want to apply for a bonus point. The deadline for these individual assignments is Friday in this week (29 May). More information can be found in the box 'Deadline individual assignments'. This week you will also receive the final assignment, that you will be working on in pairs. More information about this can be found in the box 'Final assignment'.