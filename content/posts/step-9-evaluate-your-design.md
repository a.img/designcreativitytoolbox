---
title: Step 9 - Motivate design choices
week: week 9
hint:
  - false
number: "41"
cover_image: ""
description: For every pattern that you used, write an argumentation why the
  chosen pattern suits your design. In this argumentation, focus on how the
  applied pattern facilitates a good user-system interaction (what is the
  benefit for the user).
---
For every pattern that you used, write an argumentation why the chosen pattern suits your design. In this argumentation, focus on how the applied pattern facilitates a good user-system interaction (what is the benefit for the user). 

## How to do this

Complete the motivation of your design choices you started writing in step 6. Add a motivation of why you detailed the patterns in the way you did in the visual designs. In other words: motivate the adaptation of the patterns to the specific context.



## What to present in the report

Describe the patterns you used and motivate why you selected them and explain how you applied them. Categorize the patterns per chapter from the book.