---
title: Assignment Expert review
week: week 1
hint: true
number: "5"
description: This week you will apply what you learned about design principles
  when you perform an expert review for an existing website. Read here what is
  expected from the assignment.
---
This week you will apply what you learned about design principles when you perform an expert review for an existing website. Read here what is expected from the assignment.

> **Assignment Expert review**
>
> Download the assignment [here](https://blackboard.hhs.nl/bbcswebdav/pid-2976788-dt-content-rid-25695667_2/courses/UXD-ID2-1-16-2019/Week%201-Assignment%20Expert%20review-1920.pdf).