---
title: Step 6 - Motivate the choice for patterns
week: week 7
hint:
  - false
number: "29"
cover_image: ""
description: Start writing a motivation for the choice of patterns. For now,
  focus on motivating why the chosen patterns should be applied.  You will apply
  and detail the patterns further in the next step (visual design). After you
  have finished the visual design, you can complete your motivation with a
  argumentation of why you adapted the patterns in the way you did.
---
Start writing a motivation for the choice of patterns. For now, focus on motivating why the chosen patterns should be applied.  You will apply and detail the patterns further in the next step (visual design). After you have finished the visual design, you can complete your motivation with a argumentation of why you adapted the patterns in the way you did.

## How to write?

When writing your motivation, use the book! For every patterns the book describes a 'Use when' and 'Why' section. This is valuable information to find the right words for your motivation.

## What to do with our notes?

Keep your notes for now. You can finish the motivation of selection and adaptation of design patterns once you have finished the visual designs.