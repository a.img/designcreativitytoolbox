---
title: "*** TO DO WEEK 4 ***"
week: week 4
hint:
  - false
number: "15"
cover_image: ""
description: Before the first tutor meeting of this week, study Chapter 6 and
  Chapter 8 from the book if you have the 2nd edition or Chapter 8 and Chapter
  10 if you have the 3rd edition.  After the meeting, start working on the
  assignment for this week, that you can find at 'Assignment Mobile webshop and
  registration procedure'.
---
Before the first tutor meeting of this week, study Chapter 6 and Chapter 8 from the book if you have the 2nd edition or Chapter 8 and Chapter 10 if you have the 3rd edition.  After the meeting, start working on the assignment for this week, that you can find at 'Assignment Mobile webshop and registration procedure'.