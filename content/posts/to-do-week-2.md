---
title: "*** TO DO WEEK 2 ***"
week: week 2
hint:
  - false
number: "6"
description: Before the first tutor meeting of this week, study Chapter 3 and
  Chapter 4 from the book (chapter numbers are the same for the 2nd and 3rd
  edition) and the information about Grids in interface design.  After the
  meeting, start working on the assignment for this week, that you can find at
  'Assignment Little Red Riding Hood'.
---
Before the first tutor meeting of this week, study Chapter 3 and Chapter 4 from the book (chapter numbers are the same for the 2nd and 3rd edition) and the information about Grids in interface design.  After the meeting, start working on the assignment for this week, that you can find at 'Assignment Little Red Riding Hood'.