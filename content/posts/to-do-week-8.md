---
title: "*** TO DO WEEK 8 ***"
week: week 8
hint:
  - false
number: "35"
cover_image: ""
description: To keep on track with the final assignment, we advise you to finish
  the steps presented here in week 8.
---
To keep on track with the final assignment, we advise you to finish the steps presented here in week 8.