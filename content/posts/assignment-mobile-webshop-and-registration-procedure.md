---
title: Assignment Mobile webshop and registration procedure
week: week 4
hint:
  - false
number: "17"
cover_image: ""
description: This week you will apply what you read about Doing things and
  Getting input from users to the design of a mobile version of your webshop and
  a registration procedure that fits this webshop. Read here what you are
  expected to do.
---
This week you will apply what you read about Doing things and Getting input from users to the design of a mobile version of your webshop and a registration procedure that fits this webshop. Read here what you are expected to do.

> **Assignment Mobile webshop and registration procedure**
>
> Download the assignment [here](https://blackboard.hhs.nl/bbcswebdav/pid-2976905-dt-content-rid-25696837_2/courses/UXD-ID2-1-16-2019/Week%204-Assignment%20Mobile%20webshop%20and%20registration%20procedure-1920.pdf).
>
> Download the flowchart that you have to use to design the registration procedure [here](https://blackboard.hhs.nl/bbcswebdav/pid-2976907-dt-content-rid-25696838_2/courses/UXD-ID2-1-16-2019/Week%204-Assignment%20Flowchart-1920.pdf). 
>
> Download the text that you have to use to design the registration form [here](https://blackboard.hhs.nl/bbcswebdav/pid-2976909-dt-content-rid-25696843_2/courses/UXD-ID2-1-16-2019/Week%204-Assignment%20Text%20registration%20form-1920.pdf).