---
title: Step 4 - Define the visual style for the redesign
week: week 6
hint:
  - false
number: "25"
cover_image: ""
description: Define the visual style for the redesign of the webshop by making a style tile.
---
Define the visual style for the redesign of the webshop by making a style tile. 

Include a grid and interactive elements to show how elements will react on actions like hovering and clicking, how elements will unfold (eg submenu's, dropdowns, etc), what different states of elements look like (active menu items, disabled buttons, etc).

Also describe why the visual style you designed fits the brand.

In the grey box below you can download extra information about this step.

## Downloads

> Download extra information about defining the visual style [here](https://blackboard.hhs.nl/bbcswebdav/pid-3000602-dt-content-rid-26105158_2/courses/UXD-ID2-1-16-2019/Define%20visual%20style%20-%20extra%20information.pdf).