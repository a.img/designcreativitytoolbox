---
title: "*** TO DO WEEK 9 ***"
week: week 9
hint:
  - false
number: "40"
cover_image: ""
description: To keep on track with the final assignment, we advise you to finish
  the steps presented here in week 9.
---
To keep on track with the final assignment, we advise you to finish the steps presented here in week 9.