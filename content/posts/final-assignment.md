---
title: Final assignment
week: week 5
hint:
  - false
number: "20"
cover_image: ""
description: After the second lesson of this week, you will start working (in
  pairs) on the final assignment. For the final assignment you will be
  redesigning an existing webshop. This assignment is graded and has to be
  handed in on Friday 26 June. Please read the information that is presented
  here before the second lesson of this week, so we can discuss the assignment
  during the tutor meeting.
---
After the second lesson of this week, you will start working on the final assignment in pairs. For the final assignment you will be redesigning an existing webshop. This assignment is graded and has to be handed in on Friday 26 June. Please read the information that is presented here before the second lesson of this week, so we can discuss the assignment during the tutor meeting.

## The assignment

In the final assignment for this course, you will apply the knowledge you gained in the first part of the course to a design challenge. The challenge is to make a redesign for an existing webshop. 

## Step by step plan

On this course website, we offer you a step by step plan that describes how you can approach this assignment. We also suggest a planning for this step by step plan, that should help you to keep on track. Check the information for each week for this step by step plan. You can also check [this presentation](https://blackboard.hhs.nl/bbcswebdav/pid-2976783-dt-content-rid-26105139_2/courses/UXD-ID2-1-16-2019/Final%20assignment%20step%20by%20step.pdf) for a complete overview of all steps.

## Deliverable: design report

Write a design report that consists of a description of your process and screenshots of the redesign. The table of contents for the design report is presented in [this document](https://blackboard.hhs.nl/bbcswebdav/pid-3000595-dt-content-rid-26105141_2/courses/UXD-ID2-1-16-2019/Contents%20report%20ID2.pdf).

## Deadline and grading

The deadline for this assignment is Friday 26 June (midnight). You can find the assessment form in the grey box below and in the box 'Grading' in week 10.

## Downloads

> Download the step by step plan for the final assignment report [here](https://blackboard.hhs.nl/bbcswebdav/pid-2976783-dt-content-rid-26105139_2/courses/UXD-ID2-1-16-2019/Final%20assignment%20step%20by%20step.pdf).
>
> Download the contents for the design report [here](https://blackboard.hhs.nl/bbcswebdav/pid-3000595-dt-content-rid-26105141_2/courses/UXD-ID2-1-16-2019/Contents%20report%20ID2.pdf).
>
> Download the assessment form [here](https://blackboard.hhs.nl/bbcswebdav/pid-2976781-dt-content-rid-25695643_2/courses/UXD-ID2-1-16-2019/Assessmentform-ID2-1920.pdf).