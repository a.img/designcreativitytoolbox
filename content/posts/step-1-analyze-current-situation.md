---
title: Step 1 - Analyse current situation
week: week 6
hint:
  - false
number: "22"
cover_image: ""
description: Analyse the persona, analyse the current website by performing an
  extensive expert review and present your findings in a convincing and coherent
  conclusion.
---
Analyse the persona, analyse the current website by performing an extensive expert review and present your findings in a convincing and coherent conclusion.

The analysis should be good input to formulate requirements for the redesign in step 3. In the grey box below, you can download extra information about the analysis and writing requirements based on this analysis.

## Persona

Read the persona, for download [here](https://blackboard.hhs.nl/bbcswebdav/pid-3000598-dt-content-rid-26105151_2/courses/UXD-ID2-1-16-2019/Persona.pdf). Describe the most important insights that you gained from the persona that are relevant for your design challenge.

## Expert review

Make an extensive expert review of the website, using the design principles and the ‘to consider’ themes that are described for all design principles. Extensive means: don’t limit yourself to 1 finding per principle but make sure that you cover as much as possible.

## Conclusion

Write a conclusion of your analysis of the persona and the expert review: summarize the most important insights that will guide you in redesigning the website. 

## Be aware

This is the first step to take, but be aware that the results of your work have to be presented in Chapter 2 of the design report. Chapter 1 is reserved for the introduction, that you will write after you analyzed the current situation.

## Downloads

> Download the persona [here](https://blackboard.hhs.nl/bbcswebdav/pid-3000598-dt-content-rid-26105151_2/courses/UXD-ID2-1-16-2019/Persona.pdf).
>
> Download extra information about analysis & requirements [here](https://blackboard.hhs.nl/bbcswebdav/pid-3000601-dt-content-rid-26105157_2/courses/UXD-ID2-1-16-2019/Analysis%20and%20requirements%20-%20extra%20information.pdf).