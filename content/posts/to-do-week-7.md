---
title: "*** TO DO WEEK 7 ***"
week: week 7
hint:
  - false
number: "26"
cover_image: ""
description: To keep on track with the final assignment, we advise you to finish
  the steps presented here in week 7.
---
To keep on track with the final assignment, we advise you to finish the steps presented here in week 7.