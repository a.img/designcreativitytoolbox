---
title: Course Overview
week: week 1
hint: false
number: "2"
description: This course is about learning and using interaction design
  principles and interaction design patterns to create the user interface of an
  interactive digital product. Here we explain more about the subject and topics
  in this course.
---
This course is about learning and using interaction design principles and interaction design patterns to create the user interface of an interactive digital product. Here we explain more about the subject and topics in this course.

### Subject and topics

In the course Interaction Design 1 you learned user interaction design techniques to create the (information) structure of an interactive digital product. After having created this structure, the next step in interaction design is to design the user interface for that product. 

The aim is always to create a user-friendly interface. In this course, you will learn and use interaction design principles and interaction design patterns to do so.

> ## "A user interface is like a joke. If you have to explain it, it's not that good"

In this course we define interaction design principles as broad statements about usability that guide your design efforts,  derived by evaluating common design problems across many systems. Interaction design patterns are general design solutions of proven quality. 

You will work on design projects from a given problem, in which you are designing certain parts of an interface. This may for example be an interactive navigation menu or an interactive list. You learn to analyze the problem using design principles and then choose a design pattern that fits the problem. Design patterns are general solutions and you learn to adapt these general solutions to the specific context in which they should be implemented.

This course covers the following topics: 

* Design principles and design patterns: what they are and which ones are important for interaction designers? 
* Evaluating an interface using design principles
* Choosing design patterns that fit a given problem
* Implementing and adapting a general design pattern to the specific context in which it will be used
* Designing a user interface based on a given interaction design structure (flowchart, requirements)

### Completing the course

In the first 4 weeks of this course, you will work on individual assignments that concern different categories of design patterns. These assignments facilitate that you can practise working with the design patterns, which will help you to be successful in the final assignment for this course.

The individual assignments have to be handed in at the end of week 5. If all assignments are handed in and all assignments meet all requirements, you will get 1 bonuspoint that is added to the grade you will get for this course. It’s not possible to get a higher grade than 10.

In week 5, the final assignment of this course will be presented to you. You will work on this assignment in pairs. The final assignment is the redesign of an interface for a webshop. In this design, you will put in practice the knowledge you gained in the weeks before. You will write a design report about the design and the design decisions should be well explained in accompanying comments. The design itself and the design report will be graded.

You can read more about the examination in the [study guide](https://blackboard.hhs.nl/bbcswebdav/pid-2976660-dt-content-rid-25706578_2/courses/UXD-ID2-1-16-2019/Study%20Guide-ID2-1920%281%29.pdf) (make sure you are logged in on Blackboard for a quick download of the study guide).

![](/images/uploads/userinterfacejoke.jpg)