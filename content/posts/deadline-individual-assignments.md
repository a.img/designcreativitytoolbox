---
title: Deadline individual assignments
week: week 5
hint:
  - false
number: "19"
cover_image: ""
description: If you hand in all individual assignments in time and if they meet
  all criteria, you can deserve a bonus point for this course. More information
  on this can be found here.
---
If you hand in all individual assignments in time and if they meet all criteria, you can deserve a bonus point for this course. More information on this can be found here. 

#### What about the bonus point

If all individual assignments are handed in in time, and all assignments meet all requirements, you will get 1 bonuspoint that is added to the grade you will get for this course. It’s not possible to get a higher grade than 10. ﻿﻿﻿﻿﻿

#### Deadline

The deadline for uploading everything on Blackboard is Friday 29 May (midnight).

#### What to upload

Upload a PDF document in which you put the results of all 4 individual assignment. In [this document](https://blackboard.hhs.nl/bbcswebdav/pid-2977002-dt-content-rid-25698835_2/courses/UXD-ID2-1-16-2019/Overview%20weekly%20assignments-ID2-1920%281%29.pdf), you will find an overview of all individual assignments and the criteria for each assignment.

#### Where to upload

Upload your PDF on Blackboard: Course Interaction Design 2 > Hand in > Individual assignments.