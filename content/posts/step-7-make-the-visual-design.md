---
title: Step 7 - Make the visual design
week: week 8
hint:
  - false
number: "36"
cover_image: ""
description: Make visual designs for all relevant pages and elements. Detail the
  patterns you selected in the previous step further and adapt them to this
  specific context in the visual designs.
---
Make visual designs for all relevant pages and elements. Detail the patterns you selected in the previous step further and adapt them to this specific context in the visual designs.

## Pages and elements

Make visual designs in Photoshop, Illustrator, XD, Sketch or another program that is suitable to make ‘mock-ups’. Use the wireframes as a basis for your visual design. Make sure to show at least the following pages/elements:

**Homepage**

**Product overview page** including pages/functionalities that support browsing through and searching for products (eg filtering/sorting/pagination/etc)

**Product detail page**

**All pages that are part of the check-out process** (don’t forget to include the shopping cart and confirmation page!).

## Use of patterns

Detail the patterns you have selected in the previous step further in the visual designs. Adapt them completely to this specific context. As also stated for the wireframes: make sure to use the patterns you learned in this course in your redesign. Use a minimum of 4 patterns from each Chapter in the book that was discussed in this course. 

## What to present in the report

In the design report, present the final redesign for the website of The Dutch Store, consisting of annotated screenshots of the following pages/elements. Add clear titles to the screenshots. More information about annotations can be found in the description of the next step.