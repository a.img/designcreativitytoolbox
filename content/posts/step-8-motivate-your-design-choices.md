---
title: Step 8 - Annotate your visual designs
week: week 8
hint:
  - false
number: "37"
cover_image: ""
description: Add annotations to your visual designs to make clear where you
  applied the selected patterns and how interactions work.
---
Add annotations to your visual designs to make clear where you applied the selected patterns and how interactions work.

## What to show?

The annotations should show where the patterns of your choice are applied. The annotations should also describe how interactive elements work. Think of explanations like ‘shows active state after clicking’, ‘filters are applied directly after checking them’, ‘filters are applied after the user clicks on ‘GO’’, ‘when filter X is applied, this option changes, depending on …..’, etc. 

## How to do this?

In the grey box below, you can download a presentation with some examples that show how you can annotate your designs.

## What to present in the report

In the design report, present the final redesign for the website of The Dutch Store, consisting of annotated screenshots of the following pages/elements. Add clear titles to the screenshots.



## Downloads

> Download examples of annotated design [here](https://blackboard.hhs.nl/bbcswebdav/pid-3000603-dt-content-rid-26105159_2/courses/UXD-ID2-1-16-2019/Annotating%20designs%20-%20examples.pdf).