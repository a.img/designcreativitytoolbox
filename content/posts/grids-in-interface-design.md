---
title: Grids in interface design
week: week 2
hint: no
number: "9"
description: To prepare for the first meeting of this week, next to the two
  Chapters in the book, also study the information on grids in interface design
  that is published here.
---
To prepare for the first meeting of this week, next to the two Chapters in the book, also study the information on grids in interface design that is published here.



> **Extra information on grids in interface design**
>
> Download the extra information [here](https://blackboard.hhs.nl/bbcswebdav/pid-2976805-dt-content-rid-25695700_2/courses/UXD-ID2-1-16-2019/Week%202-Extra%20Grid%20design-1920.pdf).