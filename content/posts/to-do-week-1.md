---
title: "*** TO DO WEEK 1 ***"
week: week 1
hint:
  - false
number: "1"
cover_image: ""
description: Your lecturer will invite you for a kickoff tutor meeting on
  Wednesday 29 April. Before that meeting, study the materials you can find at
  'Preparation kickoff meeting'.  After the meeting, start working on the
  assignment for this week, that you can find at 'Assignment Expert review'.
---
Your lecturer will invite you for a kickoff tutor meeting on Wednesday 29 April. Before that meeting, study the materials you can find at 'Preparation kickoff meeting'.  After the meeting, start working on the assignment for this week, that you can find at 'Assignment Expert review'.