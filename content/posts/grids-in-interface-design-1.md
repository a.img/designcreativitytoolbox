---
title: Assignment Little Red Riding Hood
week: week 2
hint: true
number: "10"
description: This week you will apply what you read about Getting around,
  Organizing the page / Layout of screen elements and Grids in interface design
  to the design of a compact website to present the story of Little Red Riding
  Hood. Read here what is expected from the assignment.
---
This week you will apply what you read about Getting around, Organizing the page / Layout of screen elements and Grids in interface design to the design of a compact website to present the story of Little Red Riding Hood. Read here what is expected from the assignment.

> **Assignment Little Red Riding Hood**
>
> Download the assignment [here](https://blackboard.hhs.nl/bbcswebdav/pid-2976807-dt-content-rid-25696601_2/courses/UXD-ID2-1-16-2019/Week%202-Assignment%20Little%20Red%20Riding%20Hood-1920.pdf).
>
> Download the text that you have to use to present the story [here](https://blackboard.hhs.nl/webapps/blackboard/execute/content/file?cmd=view&mode=designer&content_id=_2976808_1&course_id=_82286_1&framesetWrapped=true). 
>
> Download a form that you can use to give each other feedback on this assignment [here](https://blackboard.hhs.nl/bbcswebdav/pid-2976809-dt-content-rid-25696609_2/courses/UXD-ID2-1-16-2019/Week%202-Assignment%20Little%20Red%20Riding%20Hood-Peer%20feedback%20form-1920.pdf).