---
title: Step 3 - Write requirements
week: week 6
hint:
  - false
number: "24"
cover_image: ""
description: Write requirements for the redesign of the website. Link the
  requirements clearly to the insights you presented in Step 1. Make sure that
  the final list of requirements is a complete overview of what should be done
  to reach the goal you set for this design challenge in Step 2.
---
Write requirements for the redesign of the website. Link the requirements clearly to the insights you presented in Step 1. Make sure that the final list of requirements is a complete overview of what should be done to reach the goal you set for this design challenge in Step 2.

In the grey box below, you can download extra information about the analysis and writing requirements based on this analysis.



## Downloads

> Download extra information about analysis & requirement [here](https://blackboard.hhs.nl/bbcswebdav/pid-3000601-dt-content-rid-26105157_2/courses/UXD-ID2-1-16-2019/Analysis%20and%20requirements%20-%20extra%20information.pdf).