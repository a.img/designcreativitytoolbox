---
title: Step 10 - Evaluate your design
week: week 9
hint:
  - false
number: "43"
cover_image: ""
description: When the design is completely done, write an evaluation of your design.
---
When the design is completely done, write an evaluation of your design.

## How to write

Write this evaluation based on the requirements you presented in chapter 3 of your design report. Reflect on your design critically! Did you process all requirements (and if some are missing, why)? Which design choices worked out well and which ones didn’t work out that well? Have you been consistent in your design? What would you improve in a next version? Etc.

All in all you have to make clear if and how your redesign meets the goal that you stated in the introduction of the design report.